import Vue from 'vue';
import VueResource from 'vue-resource'
import Vuex from 'vuex';


Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isLoggedIn: false,
        error: null,
        token: null
    },
    getters: {
        isLoggedIn: state => {
            return state.isLoggedIn
        }
    },
    mutations: {
        LOGIN_SUCCESS (state, token) {
            state.isLoggedIn = true;
            state.token = token;
        },
        LOGOUT(state) {
            state.isLoggedIn = false;
            state.token = null
        },
        SET_AUTH_USER (state, token) {
            if (token) {
                state.token = token;
                state.isLoggedIn = true;
                console.log('залогинен');
            }
            else {
                console.log('не залогинен');
            }
        },
        SHOW_ERROR(state) {
            state.error = "Wrong email or password"
        },
        HIDE_ERROR(state) {
            state.error = undefined
        }
    },
    actions: {
        login: ({ commit }, creds) => {
            let self = this;
            let getToken = (creds) => {
                Vue.http.post('http://localhost:3030/api/login', creds).then((response) => {
                    localStorage.setItem('token', response.body.token);
                    if(response.status === 200) {
                        commit('HIDE_ERROR');
                        commit('LOGIN_SUCCESS', localStorage.token);
                    }
                }, response => {
                    commit('SHOW_ERROR')
                })
            };
            getToken(creds);
        },
        signup: ({ commit }, creds) => {
            function createUser(creds){
                Vue.http.post('http://localhost:3030/api/sign-up', creds).then((data) => {
                    localStorage.setItem('token', data.body.token);
                })
            };
            createUser(creds);
            commit('LOGIN_SUCCESS', localStorage.token);
        },
        logout: ({ commit }) => {
            localStorage.removeItem("token");
            commit('LOGOUT');
        },
        setToken: ({commit}, token) => {
            commit('SET_AUTH_USER', token)
        }
    }
});