import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import VueResource from 'vue-resource'

import Start from '@/components/Start'

import Main from '@/components/main/Main'
import MainTabs from '@/components/main/MainTabs'
import MainAddUser from '@/components/main/MainAddUser'
import MainLogin from '@/components/main/MainLogin'
import MainGrounds from '@/components/main/MainGrounds'
import MainGroundsMap from '@/components/main/MainGroundsMap'
import MainGroundItem from '@/components/main/MainGroundItem'
import MainGroundItemSchedule from '@/components/main/MainGroundItemSchedule'


import UserWindow from '@/components/user/UserWindow'
import UserAddGround from '@/components/user/UserAddGround'

import AdminWindow from '@/components/admin/AdminWindow'
import AdminAdd from '@/components/admin/AdminAdd'
import AdminMain from '@/components/admin/AdminMain'



import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Router);
Vue.use(VueResource)
Vue.use(BootstrapVue);


export default new Router({
  routes: [
    {
      path: '/', redirect: '/main'
    },
    {
      path: '/main',
      redirect: '/main/main',
      component: Start,
      children: [
        {
          path: 'grounds',
          component: MainGrounds,
          redirect: '/main/grounds/map',
          children: [
             {
              path: 'map',
              component: MainGroundsMap
            },
            {
              path: ':id',
              component: MainGroundItem
            },
          ]
        },
        {
          path: 'main',
          component: Main,
        },
        {
          path: ':id',
          component: MainGroundItem
        }
       
      ]
    },
    {
      path: '/add-user',
      component: MainAddUser,
    },
    {
      path: '/login',
      component: MainLogin,
    },
    {
      path: '/admin',
      name: 'AdminWindow',
      redirect: '/admin/admin-main',
      component: AdminWindow,
       children: [
        {
          path: 'admin-add',
          component: AdminAdd
        },
        {
          path: 'admin-main',
          component: AdminMain
        }
      ]
    },
    {
      path: '/user',
      name: 'UserWindow',
      component: UserWindow,
      children: [
        {
          path: 'add-ground',
          component: UserAddGround
        },
        
      ]
    },
  ],
  mode: 'history'
})
