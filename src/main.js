// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '../static/fonts_config.sass'
import '../static/global.sass'

import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'


Vue.http.headers.common['Authorization'] = localStorage.token;

Vue.config.productionTip = false
Vue.http.options.root = 'http://localhost:3030'


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  http: {
    emulateJSON: true,
    emulateHTTP: true
  },
  template: '<App/>',
  components: { App }
})
